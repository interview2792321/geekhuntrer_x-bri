package service

import "fmt"

func isBalancedBracket(input string) string {
	stack := []rune{}

	bracketMap := map[rune]rune{
		'}': '{',
		']': '[',
		')': '(',
	}

	for _, char := range input {
		if char == '{' || char == '[' || char == '(' {
			stack = append(stack, char)
		} else if char == '}' || char == ']' || char == ')' {
			if len(stack) == 0 || stack[len(stack)-1] != bracketMap[char] {
				return "NO"
			}
			stack = stack[:len(stack)-1]
		}
	}

	if len(stack) == 0 {
		return "YES"
	} else {
		return "NO"
	}
}

func Number2() {
	fmt.Println(isBalancedBracket("{[()]}"))
	fmt.Println(isBalancedBracket("{[(])}"))
	fmt.Println(isBalancedBracket("{(([])[])[]}"))
}
