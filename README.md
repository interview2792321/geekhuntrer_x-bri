# run server
go run main.go

# gitlab
https://gitlab.com/interview2792321/geekhuntrer_x-bri

# penjelasan nomor 2 
Kompleksitas kodingan:

Waktu: Kompleksitas waktu dari fungsi ini adalah O(n), di mana n adalah jumlah karakter dalam string input. Hanya melalui string input sekali dan melakukan operasi konstan di setiap karakter.

Ruang: Kompleksitas ruangnya adalah O(n), di mana n adalah jumlah karakter dalam string input. Menggunakan stack yang dapat tumbuh sejajar dengan jumlah kurung buka yang belum ditutup dalam string.