package service

import (
	"fmt"
	"strconv"
)

// Function to check if a number is palindrome
func isPalindrome(num int) bool {
	str := strconv.Itoa(num)
	i, j := 0, len(str)-1
	for i < j {
		if str[i] != str[j] {
			return false
		}
		i++
		j--
	}
	return true
}

// Function to recursively find the highest palindrome
func highestPalindrome(str string, k int, max *int) {
	if k < 0 {
		return
	}

	num, _ := strconv.Atoi(str)
	if isPalindrome(num) && num > *max {
		*max = num
	}

	for i := range str {
		newStr := str[:i] + string('9') + str[i+1:]
		highestPalindrome(newStr, k-1, max)
	}
}

func exec(k int, inputStr string) int {
	// inputStr := "3943"
	// k := 1

	var maxPalindrome int = -1
	highestPalindrome(inputStr, k, &maxPalindrome)

	if maxPalindrome == -1 {
		return maxPalindrome
	} else {
		return maxPalindrome
	}
}

func Number3() {
	Sample1 := exec(1, "3943")
	fmt.Println("Output 1 :", Sample1)

	Sample2 := exec(2, "932239")
	fmt.Println("Output 2 :", Sample2)
}

// Fungsi isPalindrome digunakan untuk memeriksa apakah suatu bilangan merupakan palindrome atau tidak.
// Fungsi highestPalindrome adalah fungsi rekursif yang mencoba semua kemungkinan penggantian karakter untuk menghasilkan bentuk palindrome terbesar.
