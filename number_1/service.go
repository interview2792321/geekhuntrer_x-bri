package service

import (
	"fmt"
)

func contains(s []rune, str rune) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

func uniqchar(Request string) []rune {
	var data []rune

	for _, v := range Request {
		CekContains := contains(data, v)
		switch CekContains {
		case false:
			data = append(data, v)
		}
	}
	return data
}

func weightedStrings(input string, queries []int) []string {
	// Membuat map untuk menyimpan bobot setiap karakter
	weights := map[rune]int{
		'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5,
		'f': 6, 'g': 7, 'h': 8, 'i': 9, 'j': 10,
		'k': 11, 'l': 12, 'm': 13, 'n': 14, 'o': 15,
		'p': 16, 'q': 17, 'r': 18, 's': 19, 't': 20,
		'u': 21, 'v': 22, 'w': 23, 'x': 24, 'y': 25, 'z': 26,
	}

	DataUniq := uniqchar(input)
	substringWeights := make(map[int]bool)

	// --------------------------------
	for _, v := range DataUniq {
		var NumCek int
		for _, vv := range input {
			if vv == v {
				NumCek += 1
			}
		}

		totalWeight := 0
		for i := 1; i <= NumCek; i++ {
			totalWeight += weights[v] * i
			substringWeights[totalWeight] = true
		}

	}
	// --------------------------------

	// Membuat slice untuk menyimpan hasil
	results := make([]string, len(queries))

	// Memeriksa status setiap query
	for i, query := range queries {
		if substringWeights[query] {
			results[i] = "Yes"
		} else {
			results[i] = "No"
		}
	}

	return results
}

func Number1() {
	input := "abbcccd"
	queries := []int{1, 3, 9, 8}
	exec := weightedStrings(input, queries)
	resp := fmt.Sprintf("input : %v \nqueries : %v \noutput : %v", input, queries, exec)
	fmt.Println(resp)
}
