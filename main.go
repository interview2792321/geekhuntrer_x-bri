package main

import (
	service_1 "bri/number_1"
	service_2 "bri/number_2"
	service_3 "bri/number_3"
	"fmt"
)

func main() {
	fmt.Println("-------------------- number 1 --------------------")
	service_1.Number1()
	fmt.Println("-------------------- number 2 --------------------")
	service_2.Number2()
	fmt.Println("-------------------- number 3 --------------------")
	service_3.Number3()
	fmt.Println("--------------------------------------------------")
}
